<?php
/*! \file doLog.php
 *
 *  \brief Make log entries
 *
 *  User can make log entries into the database.  Recent entries for
 *  the band and mode are shown.  Links allow updating erroneous
 *  entries.
 *
 * Pseudocode:
 * \code
 * get operator and band from arguments
 * get band name from band ID
 * get list of NWS offices for dropdown
 * put up form for entry
 * get most recent 20 log entries for this band and mode
 * display log entries with 'Change' link
 * if 'Submit'
 *   updateLog.php
 * if 'Change'
 *   editLog.php
 * \endcode
 *
 *  \author JJMcD
 *  \date 2013-09-23
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

//-------------------------------------------
// If no operator call or band, go back to 
// set call and band screen
//-------------------------------------------
if (!isset($_GET['call']))
  {
    header("Location: selectOp.php");
  }
if (!isset($_GET['band']))
  {
    header("Location: selectOp.php");
  }

//-------------------------------------------
// Get operator call and band
//-------------------------------------------
/*! Callsign of station operator (at NWS/GRR) */
$opcall = strtoupper($_GET['call']);
/*! ID of band being operated */
$bandid = $_GET['band'];

include('functions1.inc');
pageHead("Skywarn Recognition Day");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

date_default_timezone_set('America/Detroit');

/*! Query to get band name for selected band ID */
$SQL2="SELECT srd_band_a FROM srd_band_a WHERE band_id=" . $bandid;
/*! Get band name query result */
$res2=mysql_query( $SQL2, $db );
/*! First (and only) row of get band name query */
$row2=mysql_fetch_row($res2);

/*! Query for NWS office codes, locations to fill combo box */
$SQL4="SELECT srd_office_id,srd_office_code,srd_office_city,srd_office_state" .
 " FROM srd_offices_a " . 
  " ORDER BY srd_office_state, srd_office_city;";
/*! Result of get offices query */
$res4=mysql_query( $SQL4, $db );



echo "</div>\n";
//echo "<p>&nbsp;</p>\n";
echo "<p><h2>" . $opcall . " -- " . $row2[0] . "</h2>\n";
echo "</p>\n";
/*! Current time */
$now = time()+5*3600;

?>
<center>
<table width="95%">
  <tr>
   <th>Call</th>
   <th>Location</th>
   <th>Weather</th>
   <th>Office</th>
   <th>Comments</th>
  </tr>
<?php
echo "  <form method=\"post\" name=\"log\" action=\"updateLog.php?opcall=" . $opcall .
  "&bandid=" . $bandid . "\">\n";
?>
  <tr>
    <td><input type="text" name="call" length="8"/></td>
    <td><input type="text" name="location" /></td>
    <td><input type="text" name="weather" /></td>
    <td><select name="office" />
      <option value="0">Select</option>
<?php

  /*! Row from office table to fill in combo box */
  while ( $row4=mysql_fetch_row($res4) )
    {
      /*! Test of option for combo box */
      $entry=$row4[3] . " " . $row4[2] . " " . $row4[1];
      echo "      <option value=\"" . $row4[0] . "\">" . $entry . "</option>\n";
    }
?>
      </select>
    </td>
    <td><input type="text" name="comments" /></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
    <td colspan="2"><center>
      <input type="submit" value="Submit" /></center>
    </td>
  </tr>
  </form>
</table>

<hr />

<form method="get" name="loglist" action="index.php" >
<table width="95%">
  <tr>
   <th>Time</th>
   <th>Call</th>
   <th>Location</th>
   <th>Weather</th>
   <th>Office</th>
   <th>Comments</th>
   <th>Edit</th>
  <tr>
<?php

/*! Query to get most recent QSOs for this band and mode */
$SQL1="SELECT * FROM srd_log " .
  "WHERE srd_band_id=" . $bandid . " " .
  "ORDER BY srd_time DESC " .
  "LIMIT 0,20;";
/*! Result of recent QSOs query */
$res1=mysql_query( $SQL1, $db );
/*! Recent QSO row from log table */
while ( $row1=mysql_fetch_row( $res1 ) )
{
  if ( $row1[5] == 0 )
  {
    /*! NWS Office code for log display */
    $nwsoffice="";
  }
  else
  {
    /*! Query to get office code for a given ID */
    $SQL3="SELECT srd_office_code FROM srd_offices_a " .
      "WHERE srd_office_id=" . $row1[5];
    /*! Result of office code by ID query */
    $res3=mysql_query( $SQL3, $db );
    /*! Code of NWS office */
    $row3 = mysql_fetch_row($res3);
    $nwsoffice = $row3[0];
  }
  echo "  <tr>\n";
  echo "    <td>" . substr($row1[0],11,5) . "</td>\n";
  echo "    <td>" . $row1[1] . "</td>\n";
  echo "    <td>" . $row1[2] . "</td>\n";
  echo "    <td>" . $row1[4] . "</td>\n";
  if ( $nwsoffice=='' )
    echo "    <td>&nbsp;</td>\n";
  else
    echo "    <td class=\"office\">" . $nwsoffice . "</td>\n";
  echo "    <td>" . $row1[6] . "</td>\n";
  echo "    <td class=\"io\"><a href=\"editLog.php?time=" . $row1[0] . 
                "&op=" . $opcall . "&band=" . $bandid . "\">Change</a></td>\n";
//  echo "    <td align=\"center\"><input type=\"submit\" value=\"Change\" onclick=\"".
//                "this.form.action='editLog.php?time=" . $row1[0] . 
//                "&op=" . $opcall . "&band=" . $bandid . "'\" ></td>\n";
  echo "  <tr>\n";
}
echo "  </table>\n";
//echo "    <p><input type=\"submit\" value=\"Update\" />\n";
echo "      &nbsp;<input type=\"submit\" value=\"Return to Menu\" \n";
echo "      onclick=\"this.form.action='index.php'\"  /></p>\n";
echo "</center>\n";
echo "</form>\n";
pageFoot();
?>

