/*! \file makeLogEntries.c
 *
 * \brief Make bogus log entries for testing
 *
 * This program generates SQL output to populate the `srd_log`
 * table with bogus entries for testing.
 *
 * The application has 10 US prefixes, 10 local operator calls,
 * 14 weather reports, and 123 office locations.  Entries are
 * generated randomly from these lists with remote operator
 * suffixes also generated randomly.
 *
 * \author John J. McDonough
 * \date 24-Oct-2013
 *
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define TIME_DIVISOR 214748364   /*!< Used to scale rand() to time value */
//#define OFFICE_DIVISOR 17459216
#define OFFICE_DIVISOR 17459217  /*!< Used to scale rand() to office index    */
#define OPER_DIVISOR 357913941   /*!< Used to scale rand() to operator index  */
#define CALL_DIVISOR 214748364   /*!< Used to scale rand() to call index      */
#define LETTER_DIVISOR 82595524  /*!< Used to scale rand() to a letter        */
#define BAND_DIVISOR 214748364   /*!< Used to scale rand() to a band ID       */
#define WX_DIVISOR 153391689     /*!< Used to scale rand() to a weather index */

/*! List of U.S. prefixes */
char prefixes[10][10] = {
  "W", "K", "N", "WA", "WB", "KA", "KC", "AB", "KB", "WD" };

/*! List of local operators */
char operators[10][10] = {
  "N8JSN",  "AC8AL",  "WD8USA", "N8OSL", "WA8Y",
  "W8YZ", "WD8QXZ", "KA8DDQ", "NX8A", "WX8GRR" };

/*! String for weather */
char szWeather[64];

/*! List of possible weather reports */
char weathers[14][64] = {
      "Sunny and warm",
      "Clear and cold",
      "Raining",
      "Dreary",
      "High wind",
      "Cool and overcast",
      "Thunderstorms",
      "Hot and humid",
      "Hot and dry",
      "Cool with a light breeze",
      "Snow",
      "Ice storms",
      "Partly cloudy with light flurries",
      "Pleasant"
	};

/*! List of NWS office locations */
char OfficeLocs[124][32] = {
        "NULL",
	"Boulder, CO",
	"Grand Junction, CO",
	"Pueblo, CO",
	"Lincoln, IL",
	"Chicago, IL",
	"Indianapolis, IN",
	"Syracuse, IN",
	"Des Moines, IA",
	"Davenport, IA",
	"Dodge City, KS",
	"Goodland, KS",
	"Topeka, KS",
	"Wichita, KS",
	"Jackson, KY",
	"Louisville, KY",
	"Paducah, KY",
	"White Lake, MI",
	"Grand Rapids, MI",
	"Marquette, MI",
	"Gaylord, MI",
	"Duluth, MN",
	"Chanhassen, MN",
	"Pleasant Hill, MO",
	"Springfield, MO",
	"St. Louis, MO",
	"Hastings, NE",
	"North Platte, NE",
	"Valley, NE",
	"Bismark, ND",
	"Grand Forks, ND",
	"Aberdeen, SD",
	"Rapid City, SD",
	"Sioux Falls, SD",
	"Green Bay, WI",
	"La Crosse, WI",
	"Dousman, WI",
	"Cheyenne, WY",
	"Riverton, WY",
	"Portland, ME",
	"Caribou, ME",
	"Baltimore, MD",
	"Boston, MA",
	"Mount Holly, NJ",
	"Albany, NY",
	"Binghamton, NY",
	"Buffalo, NY",
	"New York, NY",
	"Morehead City, NC",
	"Raleigh, NC",
	"Wilmington, NC",
	"Wilmington, OH",
	"Cleveland, OH",
	"Charleston, SC",
	"Columbia, SC",
	"Greer, SC",
	"State College, PA",
	"Moon Township, PA",
	"Burlington, VT",
	"Blacksburg, VA",
	"Wakefield, VA",
	"Charleston, WV",
	"Calera, AL",
	"Huntsville, AL",
	"Mobile, AL",
	"Little Rock, AR",
	"Jacksonville, FL",
	"Key West, FL",
	"Melbourne, FL",
	"Miami, FL",
	"Tallahassee, FL",
	"Ruskin, FL",
	"Peachtree City, GA",
	"Lake Charles, LA",
	"Slidell, LA",
	"Shreveport, LA",
	"Jackson, MS",
	"Albuquerque, NM",
	"Norman, OK",
	"Tulsa, OK",
	"Carolina, PR",
	"Morristown, TN",
	"Memphis, TN",
	"Old Hickory, TN",
	"Amarillo, TX",
	"New Braunfels, TX",
	"Brownsville, TX",
	"Corpus Christi, TX",
	"Fort Worth, TX",
	"El Paso, TX",
	"Dickinson, TX",
	"Lubbock, TX",
	"Midland, TX",
	"San Angelo, TX",
	"Bellemont, AZ",
	"Phoenix, AZ",
	"Tucson, AZ",
	"Eureka, CA",
	"Oxnard, CA",
	"Sacramento, CA",
	"San Diego, CA",
	"Monterey, CA",
	"Hanford, CA",
	"Boise, ID",
	"Pocatello, ID",
	"Billings, MT",
	"Glasgow, MT",
	"Great Falls, MT",
	"Missoula, MT",
	"Elko, NV",
	"Las Vegas, NV",
	"Reno, NV",
	"Medford, OR",
	"Pendleton, OR",
	"Portland, OR",
	"Salt Lake City, UT",
	"Seattle, WA",
	"Spokane, WA",
	"Anchorage, AK",
	"Fairbanks, AK",
	"Juneau, AK",
	"Honolulu, HI",
	"Barrigada, GU",
	"Pago Pago, AS"
  };


char szCall[10];     /*!< Call sign in log entry */
char szLocation[32]; /*!< Location in log entry */
char nwsOffice[16];  /*!< Office code in log entry */
int maxOffice;       /*!< Maximum office ID */

/*! Randomly choose a weather report
 *
 *  Selects a weather report from the list of available
 *  weather reports, if a random number is larger than
 *  WX_DIVISOR.
 *
 * \param none
 * \returns none
 */
void setWeather( void )
{
  int wxIndex;

  strcpy(szWeather,"NULL");
  if ( rand() > WX_DIVISOR )
   {
     wxIndex = rand()/WX_DIVISOR;
     sprintf(szWeather,"'%s'",weathers[wxIndex]);
   }
}
/*! Choose an NWS office ID (which implies a location)
 *
 * If a random number is less than OPER_DIVISOR, randomly select
 * an office ID. 
 *
 * \param none
 * \returns none
 */
void setLocation( void )
{
  int nOfficeID;

  strcpy(szLocation,"NULL");
  strcpy(nwsOffice,"NULL");
  
  if ( rand() < OPER_DIVISOR )
    {
      nOfficeID = rand()/OFFICE_DIVISOR;
      if ( nOfficeID>maxOffice )
	maxOffice = nOfficeID;
      printf("-- %d\n",nOfficeID);
      if ( nOfficeID > 0 )
	{
          sprintf(nwsOffice,"%d",nOfficeID);
	  sprintf(szLocation,"'%s'",OfficeLocs[nOfficeID]);
	}
    }
}

/*! Create a bogus U.S. callsign
 *
 *  Create a callsign with a valid U.S. prefix and a 3 letter
 *  random suffix.
 *
 * \param none
 * \returns none
 */
void inventCall( void )
{
  int callarea;
  int prefixindex;
  int li1, li2, li3;

  memset(szCall,0,sizeof(szCall));
  prefixindex = rand()/CALL_DIVISOR;
  callarea = rand()/CALL_DIVISOR;
  li1 = 'A' + rand()/LETTER_DIVISOR;
  li2 = 'A' + rand()/LETTER_DIVISOR;
  li3 = 'A' + rand()/LETTER_DIVISOR;
  sprintf(szCall,"%s%d%c%c%c",prefixes[prefixindex],callarea,
	  li1,li2,li3);
}

/*! Generate SQL for bogus srd_log entries */
void main()
{
  int mins;
  int hours;
  int i;
  int delta;

  hours = 0;
  mins = 0;

  while ( hours<24 )
    {
      delta = rand() / TIME_DIVISOR;
      mins = mins + delta;
      while ( mins>60 )
	{
	  mins -= 60;
	  hours ++;
	}
      inventCall();
      setLocation();
      setWeather();
//      printf("%6d %02d:%02d %s %s\n",delta,hours,mins,
//	       operators[rand()/OPER_DIVISOR],szCall);
      printf("INSERT INTO srd_log VALUES('2013-09-24 %02d:%02d:00','%s',%s,%d,%s,%s,NULL,'%s',now());\n",
	       hours,mins,szCall,szLocation,rand()/BAND_DIVISOR,szWeather,nwsOffice,operators[rand()/OPER_DIVISOR]);

    }
  printf("--\n-- Max office = %d\n--\n",maxOffice);
}
