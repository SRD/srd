/*! \file makeBands.c
 *
 *  \brief Make SQL statements for bands and modes
 *
 *  Loop tnrough the list of bands, for each band, loop through
 *  the list of modes. Generate a SQL INSERT statement for each
 *  band and mode, except for a few invalid combinations (e.g.
 *  FM on 80 meters).
 *
 *  \author John J. McDonough WB8RCR
 *  \date 7-Nov-2013
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/
#include <stdio.h>

/*! Number of bands in band array */
#define NBANDS 15
/*! Number of modes in mode array */
#define NMODES 11

/*! List of bands */
int nBands[NBANDS] = { 
  160, 80, 40, 30, 20, 
  17, 15, 12, 10, 6, 
  2, 220, 440, 900, 1296 };

/*! List of modes */
char szModes[11][16] = {
  "USB",
  "CW",
  "FM",
  "RTTY",
  "PSK",
  "Olivia",
  "MT-63",
  "Contestia",
  "Domino",
  "Hellschreiber",
  "DV"};

/*! Make SQL statements for band/mode table */
int main()
{
  int nIndex;
  int i,j;

  nIndex = 0;
  for ( i=0; i<NBANDS; i++ )
    for ( j=0; j<NMODES; j++ )
      {
	if ( j<(NMODES-1) )
	  {
	    if ( (j!=2) || (i>7) )
	      {
		nIndex++;
		printf("INSERT INTO `srd_band_a` VALUES(%d,'%d %s',now());\n",
		       nIndex, nBands[i], szModes[j]);
	      }
	  }
	else
	  if ( i>9 )
	    {
	      nIndex++;
	      printf("INSERT INTO `srd_band_a` VALUES(%d,'%d %s',now());\n",
		     nIndex, nBands[i], szModes[j]);
	    }
	
      }
  return 0;
}
