/*! \file cvtCWA.c
 *
 *  \brief Convert .csv of NWS offices to SQL
 *
 *  Program reads \c NWS_CWA.csv and generates a list of
 *  \c UPDATE commands to add the office's location (actually
 *  the center of the office's CWA) into the database.
 *
 *  \author John J. McDonough WB8RCR
 *  \date 7-Nov-2013
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

#include <stdio.h>
#include <string.h>

/*! File to open */
#define FILENAME "NWS_CWA.csv"

/*! Buffer for reading file */
char szBuffer[128];
/*! Three-letter office code */
char szCWA[16];
/*! Latitude of office */
char szLat[16];
/*! Longitude of office */
char szLon[16];

/*! Convert csv of office locations to SQL */
void main( void )
{
  FILE *f;
  char *p,*q;

  f = fopen(FILENAME,"r");
  while ( !feof(f) )
    {
      fgets(szBuffer,sizeof(szBuffer),f);
      if ( !feof(f) )
	{
	  p = strchr(szBuffer,',');
	  *p=0;
	  strcpy(szCWA,szBuffer);
	  p++;
	  q = strchr(p,',');
	  *q=0;
	  strcpy(szLon,p);
	  q++;
	  q[strlen(q)-1]=0;
	  strcpy(szLat,q);
	  printf("UPDATE `srd_offices_a` SET `srd_office_x`=%s, `srd_office_y`=%s WHERE `srd_office_code`='%s';\n",
		 szLon,szLat,szCWA);
	}

    }
}
