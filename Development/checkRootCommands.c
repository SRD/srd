/*! \file checkRootCommands.c
 *
 *  \brief Do privileged commands set up by web
 *
 *  checkRootCommands is run as a service called
 *  \r rootcommands.service
 *
 *  The application checks once every 10 seconds to see
 *  whether \d DATEFILE exists.  If it does, the contents are
 *  prepended with \c date \c -s and executed as a command.
 *
 *  It then checks whether \c DOWNFILE exists.  If it does, the
 *  command \c shutdown \c -h \c now is executed.
 *
 *  Pseudocode
 *  \code
 *  do forever
 *    if exists DATEFILE
 *      read DATEFILE
 *      do date -s (contents of DATEFILE)
 *    if exists DOWNFILE
 *      shutdown -h now
 *  end do
 *  \endcode
 *
 *  \author John J. McDonough WB8RCR
 *  \date 7-Nov-2013
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

#include <stdio.h>
#include <string.h>

/*! Name of the file for setting date/time */
#define DATEFILE "/var/www/html/srd/timetoset"
/*! Name of the file to initiate shutdown */
#define DOWNFILE "/var/www/html/srd/shutdown"
/*! Name of the log file */
#define LOGFILE "/root/commands.log"

/*! Buffer for reading file */
char szBuffer[1024];
/*! Command to execute */
char szCommand[1024];

/*! Log the command */
void logit( char *szCommand )
{
  FILE *g;

  fprintf(stderr,"checkRootCommands: Command=[%s]\n",szCommand);
  g = fopen(LOGFILE,"w");
  fprintf(g,"Cmd:[%s]\n",szCommand);
  fclose(g);
}

/*! Execute specific root commands */
void main()
{
  FILE *f;

  while (1) 
    {
      f = fopen(DATEFILE,"r");
      if ( f )
	{
	  fgets(szBuffer,sizeof(szBuffer),f);
	  strcpy(szCommand,"date -s '");
	  strcat(szCommand,szBuffer);
	  strcat(szCommand,"'");
	  fclose(f);
	  logit(szCommand);
	  unlink(DATEFILE);
	  system(szCommand);
	}
      //      else
      //	perror("Datefile");
      f = fopen(DOWNFILE,"r");
      if ( f )
	{
	  fclose(f);
	  unlink(DOWNFILE);
	  strcpy(szCommand,"shutdown -h now");
	  logit(szCommand);
	  system(szCommand);
	}
      //      else
      //	perror("Downfile");

      //      fprintf(stderr,"Sleeping\n");
      sleep(10);
    }
}
