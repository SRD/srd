sq-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: cimbaoth
-- Generation Time: Oct 24, 2013 at 12:21 PM
-- Server version: 5.5.33
-- PHP Version: 5.4.20

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `SRD`
--
use SRD;


-- --------------------------------------------------------

--
-- Table structure for table `srd_band_a`
--
-- Creation: Sep 24, 2013 at 01:10 PM
--

DROP TABLE IF EXISTS `srd_band_a`;
CREATE TABLE IF NOT EXISTS `srd_band_a` (
  `band_id` int(11) NOT NULL,
  `srd_band_a` char(24) COLLATE utf8_unicode_ci NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `srd_band_a_idx` (`band_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `srd_band_a`
--

INSERT INTO `srd_band_a` VALUES(1,'160 LSB',now());
INSERT INTO `srd_band_a` VALUES(2,'160 CW',now());
INSERT INTO `srd_band_a` VALUES(3,'160 RTTY',now());
INSERT INTO `srd_band_a` VALUES(4,'160 PSK',now());
INSERT INTO `srd_band_a` VALUES(5,'160 Olivia',now());
INSERT INTO `srd_band_a` VALUES(6,'160 MT-63',now());
INSERT INTO `srd_band_a` VALUES(7,'160 Contestia',now());
INSERT INTO `srd_band_a` VALUES(8,'160 Domino',now());
INSERT INTO `srd_band_a` VALUES(9,'160 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(10,'80 LSB',now());
INSERT INTO `srd_band_a` VALUES(11,'80 CW',now());
INSERT INTO `srd_band_a` VALUES(12,'80 RTTY',now());
INSERT INTO `srd_band_a` VALUES(13,'80 PSK',now());
INSERT INTO `srd_band_a` VALUES(14,'80 Olivia',now());
INSERT INTO `srd_band_a` VALUES(15,'80 MT-63',now());
INSERT INTO `srd_band_a` VALUES(16,'80 Contestia',now());
INSERT INTO `srd_band_a` VALUES(17,'80 Domino',now());
INSERT INTO `srd_band_a` VALUES(18,'80 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(19,'40 LSB',now());
INSERT INTO `srd_band_a` VALUES(20,'40 CW',now());
INSERT INTO `srd_band_a` VALUES(21,'40 RTTY',now());
INSERT INTO `srd_band_a` VALUES(22,'40 PSK',now());
INSERT INTO `srd_band_a` VALUES(23,'40 Olivia',now());
INSERT INTO `srd_band_a` VALUES(24,'40 MT-63',now());
INSERT INTO `srd_band_a` VALUES(25,'40 Contestia',now());
INSERT INTO `srd_band_a` VALUES(26,'40 Domino',now());
INSERT INTO `srd_band_a` VALUES(27,'40 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(29,'30 CW',now());
INSERT INTO `srd_band_a` VALUES(31,'30 PSK',now());
INSERT INTO `srd_band_a` VALUES(37,'20 USB',now());
INSERT INTO `srd_band_a` VALUES(38,'20 CW',now());
INSERT INTO `srd_band_a` VALUES(39,'20 RTTY',now());
INSERT INTO `srd_band_a` VALUES(40,'20 PSK',now());
INSERT INTO `srd_band_a` VALUES(41,'20 Olivia',now());
INSERT INTO `srd_band_a` VALUES(42,'20 MT-63',now());
INSERT INTO `srd_band_a` VALUES(43,'20 Contestia',now());
INSERT INTO `srd_band_a` VALUES(44,'20 Domino',now());
INSERT INTO `srd_band_a` VALUES(45,'20 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(46,'17 USB',now());
INSERT INTO `srd_band_a` VALUES(47,'17 CW',now());
INSERT INTO `srd_band_a` VALUES(48,'17 RTTY',now());
INSERT INTO `srd_band_a` VALUES(49,'17 PSK',now());
INSERT INTO `srd_band_a` VALUES(50,'17 Olivia',now());
INSERT INTO `srd_band_a` VALUES(51,'17 MT-63',now());
INSERT INTO `srd_band_a` VALUES(52,'17 Contestia',now());
INSERT INTO `srd_band_a` VALUES(53,'17 Domino',now());
INSERT INTO `srd_band_a` VALUES(54,'17 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(55,'15 USB',now());
INSERT INTO `srd_band_a` VALUES(56,'15 CW',now());
INSERT INTO `srd_band_a` VALUES(57,'15 RTTY',now());
INSERT INTO `srd_band_a` VALUES(58,'15 PSK',now());
INSERT INTO `srd_band_a` VALUES(59,'15 Olivia',now());
INSERT INTO `srd_band_a` VALUES(60,'15 MT-63',now());
INSERT INTO `srd_band_a` VALUES(61,'15 Contestia',now());
INSERT INTO `srd_band_a` VALUES(62,'15 Domino',now());
INSERT INTO `srd_band_a` VALUES(63,'15 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(64,'12 USB',now());
INSERT INTO `srd_band_a` VALUES(65,'12 CW',now());
INSERT INTO `srd_band_a` VALUES(66,'12 RTTY',now());
INSERT INTO `srd_band_a` VALUES(67,'12 PSK',now());
INSERT INTO `srd_band_a` VALUES(68,'12 Olivia',now());
INSERT INTO `srd_band_a` VALUES(69,'12 MT-63',now());
INSERT INTO `srd_band_a` VALUES(70,'12 Contestia',now());
INSERT INTO `srd_band_a` VALUES(71,'12 Domino',now());
INSERT INTO `srd_band_a` VALUES(72,'12 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(73,'10 USB',now());
INSERT INTO `srd_band_a` VALUES(74,'10 CW',now());
INSERT INTO `srd_band_a` VALUES(75,'10 FM',now());
INSERT INTO `srd_band_a` VALUES(76,'10 RTTY',now());
INSERT INTO `srd_band_a` VALUES(77,'10 PSK',now());
INSERT INTO `srd_band_a` VALUES(78,'10 Olivia',now());
INSERT INTO `srd_band_a` VALUES(79,'10 MT-63',now());
INSERT INTO `srd_band_a` VALUES(80,'10 Contestia',now());
INSERT INTO `srd_band_a` VALUES(81,'10 Domino',now());
INSERT INTO `srd_band_a` VALUES(82,'10 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(83,'6 USB',now());
INSERT INTO `srd_band_a` VALUES(84,'6 CW',now());
INSERT INTO `srd_band_a` VALUES(85,'6 FM',now());
INSERT INTO `srd_band_a` VALUES(86,'6 RTTY',now());
INSERT INTO `srd_band_a` VALUES(87,'6 PSK',now());
INSERT INTO `srd_band_a` VALUES(88,'6 Olivia',now());
INSERT INTO `srd_band_a` VALUES(89,'6 MT-63',now());
INSERT INTO `srd_band_a` VALUES(90,'6 Contestia',now());
INSERT INTO `srd_band_a` VALUES(91,'6 Domino',now());
INSERT INTO `srd_band_a` VALUES(92,'6 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(93,'2 USB',now());
INSERT INTO `srd_band_a` VALUES(94,'2 CW',now());
INSERT INTO `srd_band_a` VALUES(95,'2 FM',now());
INSERT INTO `srd_band_a` VALUES(96,'2 RTTY',now());
INSERT INTO `srd_band_a` VALUES(97,'2 PSK',now());
INSERT INTO `srd_band_a` VALUES(98,'2 Olivia',now());
INSERT INTO `srd_band_a` VALUES(99,'2 MT-63',now());
INSERT INTO `srd_band_a` VALUES(100,'2 Contestia',now());
INSERT INTO `srd_band_a` VALUES(101,'2 Domino',now());
INSERT INTO `srd_band_a` VALUES(102,'2 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(103,'2 DV',now());
INSERT INTO `srd_band_a` VALUES(104,'220 USB',now());
INSERT INTO `srd_band_a` VALUES(105,'220 CW',now());
INSERT INTO `srd_band_a` VALUES(106,'220 FM',now());
INSERT INTO `srd_band_a` VALUES(107,'220 RTTY',now());
INSERT INTO `srd_band_a` VALUES(108,'220 PSK',now());
INSERT INTO `srd_band_a` VALUES(109,'220 Olivia',now());
INSERT INTO `srd_band_a` VALUES(110,'220 MT-63',now());
INSERT INTO `srd_band_a` VALUES(111,'220 Contestia',now());
INSERT INTO `srd_band_a` VALUES(112,'220 Domino',now());
INSERT INTO `srd_band_a` VALUES(113,'220 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(114,'220 DV',now());
INSERT INTO `srd_band_a` VALUES(115,'440 USB',now());
INSERT INTO `srd_band_a` VALUES(116,'440 CW',now());
INSERT INTO `srd_band_a` VALUES(117,'440 FM',now());
INSERT INTO `srd_band_a` VALUES(118,'440 RTTY',now());
INSERT INTO `srd_band_a` VALUES(119,'440 PSK',now());
INSERT INTO `srd_band_a` VALUES(120,'440 Olivia',now());
INSERT INTO `srd_band_a` VALUES(121,'440 MT-63',now());
INSERT INTO `srd_band_a` VALUES(122,'440 Contestia',now());
INSERT INTO `srd_band_a` VALUES(123,'440 Domino',now());
INSERT INTO `srd_band_a` VALUES(124,'440 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(125,'440 DV',now());
INSERT INTO `srd_band_a` VALUES(126,'900 USB',now());
INSERT INTO `srd_band_a` VALUES(127,'900 CW',now());
INSERT INTO `srd_band_a` VALUES(128,'900 FM',now());
INSERT INTO `srd_band_a` VALUES(129,'900 RTTY',now());
INSERT INTO `srd_band_a` VALUES(130,'900 PSK',now());
INSERT INTO `srd_band_a` VALUES(131,'900 Olivia',now());
INSERT INTO `srd_band_a` VALUES(132,'900 MT-63',now());
INSERT INTO `srd_band_a` VALUES(133,'900 Contestia',now());
INSERT INTO `srd_band_a` VALUES(134,'900 Domino',now());
INSERT INTO `srd_band_a` VALUES(135,'900 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(136,'900 DV',now());
INSERT INTO `srd_band_a` VALUES(137,'1296 USB',now());
INSERT INTO `srd_band_a` VALUES(138,'1296 CW',now());
INSERT INTO `srd_band_a` VALUES(139,'1296 FM',now());
INSERT INTO `srd_band_a` VALUES(140,'1296 RTTY',now());
INSERT INTO `srd_band_a` VALUES(141,'1296 PSK',now());
INSERT INTO `srd_band_a` VALUES(142,'1296 Olivia',now());
INSERT INTO `srd_band_a` VALUES(143,'1296 MT-63',now());
INSERT INTO `srd_band_a` VALUES(144,'1296 Contestia',now());
INSERT INTO `srd_band_a` VALUES(145,'1296 Domino',now());
INSERT INTO `srd_band_a` VALUES(146,'1296 Hellschreiber',now());
INSERT INTO `srd_band_a` VALUES(147,'1296 DV',now());

SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
