<?php
/*! \file selectOp.php
 *
 *  \brief Select operator and band for SRD log
 *
 *  \author JJMcD
 *  \date 2013-09-24  
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Set Operator");
echo "    </div>\n";
echo "    <p>&nbsp;</p>\n";

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

//-------------------------------------------
// Get list of bands we want to work
//-------------------------------------------
$SQL1="SELECT band_id, srd_band_a FROM srd_band_a ORDER BY band_id";
//echo "<p>" . $SQL1 . "</p>\n";
$res1=mysql_query( $SQL1, $db );

echo "    <center>\n";
echo "    <div>\n";
echo "      <form method=\"get\" action=\"doLog.php?nws=NULL\">\n";
echo "        <table>\n";
echo "          <tr>\n";
echo "            <th align=\"right\">&nbsp;Enter operator call sign&nbsp;</th>\n";
echo "            <td><input  type=\"text\" name=\"call\" /></td>";
echo "          </tr>\n";
echo "          <tr>\n";
echo "            <th align=\"right\">&nbsp;Select band&nbsp;</th>\n";
echo "              <td><select name=\"band\">\n";
echo "                <option value=\"#\">Select Band</option>\n";
while ( $row1=mysql_fetch_row( $res1 ) )
  {
    echo "                <option value=\"" . $row1[0] . "\">" . $row1[1] . 
         "</option>\n";
  }
echo "              </select></td>\n";
echo "          </tr>\n";

echo "      </table>\n";
echo "      <p><input type=\"submit\" value=\"Log Contacts\" />\n";
echo "        &nbsp;<input type=\"submit\" value=\"Return to Menu\" \n";
echo "        onclick=\"this.form.action='index.php'\"  /></p>\n";
echo "    </form>\n";
echo "  </div>\n";
echo "</center>\n";
pageFoot();
?>

