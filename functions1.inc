<?php
/*! \file functions1.inc
 *
 *  \brief Functions used for the application
 *
 *  This file provides a number of functions which are used
 *  throughout the application.  Included are functions for
 *  connecting to the database, displaying database error
 *  messages, displaying page headers and footers, and providing
 *  some standard formatting for specific fields.
 *
 *  \author JJMcD
 *  \date 2012-12-30
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

//! Get the text associated with the most recent MySQL error
/*! Gets the error message text from MySQL and formats it into a
 *  string for display, including a color indicating the severity.
 *
 * \code
 * Pseucocode:
 * switch ( $type )
 * case $type='error'
 *    $Colour='red'
 *    if ( $prefix is blank )
 *      $prefix = ERROR Message Report
 * case $type='warn'
 *    $Colour='maroon'
 *    if ( $prefix is blank )
 *      $prefix = WARNING Message Report
 * case $type='success'
 *    $Colour='dark green'
 *    if ( $prefix is blank )
 *      $prefix = SUCCESS Report
 * case $type='info' OR anything else
 *    $Colour='navy'
 *    if ( $prefix is blank )
 *      $prefix = INFORMATION Message
 * return a string with HTML for the formatted message
 * \endcode
 *
 *  \param $msg - Message text to display
 *  \param $type - Message severity, default = info
 *  \param $prefix - Prefix for the message, default = empty
 *  \returns formatted string
 */
function getMsg($msg,$type='info',$prefix='')
  {
    /*! Color in which to display the message */
    $Colour='';
    switch($type)
      {
	case 'error':
	     $Colour='red';
	     $prefix = $prefix ? $prefix : _('ERROR') . ' ' .
	     	     _('Message Report');
	     break;
        case 'warn':
	     $Colour='maroon';
	     $prefix = $prefix ? $prefix : _('WARNING') . ' ' . 
	     	     _('Message Report');
	     break;
	case 'success':
	     $Colour='darkgreen';
	     $prefix = $prefix ? $prefix : _('SUCCESS') . ' ' . _('Report');
	      break;
        case 'info':
	default:
		$prefix = $prefix ? $prefix : _('INFORMATION') . ' ' .
			_('Message');
                $Colour='navy';
      }
    return '<font color="' . $Colour . '"><B>' . $prefix . '</B> : ' .
    	   $msg . '</font>';
  }//getMsg

//! Display an error message on the page 
/*! This function calls getMsg and displays the resulting text in a paragraph
 *
 *  \param $msg - Message text to display
 *  \param $type - Message severity, default = info
 *  \param $prefix - Prefix for the message, default = empty
 *  \returns none
 */
function prnMsg($msg,$type='info', $prefix='')
  {
    echo '<P>' . getMsg($msg, $type, $prefix) . '</P>';
  }//prnMsg

//! Display the top of all pages
/*! Opens the HTML and head section, provides the title to appear
 *  in the titlebar and all of the <head> section, including reference
 *  to the style sheet.  Also writes out the opening <div> and the
 *  page title at the top of the page.
 *
 *  \param $title - Text to appear in the titlebar and page heading
 *  \returns none
 */
function pageHead( $title )
  {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" " .
    	 "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" " .
    	 "xml:lang=\"en\">\n";
    echo "  <head>\n    <title>NWS GRR " . $title . 
    	 "</title>\n";
    echo "    <style type=\"text/css\" media=\"all\">@import " .
    	 "\"default1.css\";</style>\n";
    echo "  </head>\n  <body>\n";
    echo "    <div id=\"headarea\">\n";
    echo "      <h2>" . $title . "</h2>\n";
  }//pageHead

//! Display the top of refreshing pages
/*! Opens the HTML and head section, provides the title to appear
 *  in the titlebar and all of the <head> section, including reference
 *  to the style sheet.  Also writes out the opening <div> and the
 *  page title at the top of the page.  This version also instructs
 *  the page to refresh every two minutes.  Refreshing pages also
 *  include the Essexville Electric artwork.
 *
 *  \param $title - Text to appear in the titlebar and page heading
 *  \returns none
 */
function pageHeadR( $title )
  {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" " .
    	 "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" " .
    	 "xml:lang=\"en\">\n";
    echo "  <head>\n    <title>NWS Grand Rapids " . $title . 
    	 "</title>\n";
    echo "    <style type=\"text/css\" media=\"all\">@import " .
    	 "\"default1.css\";</style>\n";
    echo "    <meta http-equiv=\"refresh\" content=\"60\" />\n";
    echo "  </head>\n  <body>\n";
    echo "    <div id=\"headarea\">\n";
    echo "    <center><h2>" . $title . "</h2></center>\n";
  }//pageHeadR

//! Display the page footer
/*! This function displays the license tag and then provides
 *  the closing HTML tags for the page.
 *
 *  \param none
 *  \returns none
 */
function pageFoot( )
  {
    echo "    <div id=\"footer\">\n";
    echo "      <p>&nbsp;</p>\n";
    echo "      <p>Licensed under the GNU Public License V2.0</p>\n";
    echo "    </div>\n";
    echo "  </body>\n";
    echo "</html>\n";
  }

//! Open the database
/*! This function opens a handle to the database server using the
 *  default SRD server credentials.  It then selects the SRD  database.
 *
 *  If the database open fails an error message is generated.
 *
 *  \param none
 *  \returns Database handle
 */
function openDatabase( )
  {
    $db = mysql_connect( 'localhost', 'srduser', 'srdpassword' );
    mysql_select_db( 'SRD', $db );

    if (mysql_errno($db) != 0 )
      {
        prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error',
	     _('Database Error')); 
      }
    return $db;
  }

//! Display the last updated cell as a smaller font
/*! This page outputs a table cel with a style that centers the
 *  content in a smaller font.  It is used for displaying the
 *  last update time on the status display.
 *
 *  \param $Updated - text for the cell
 *  \returns none
 */
function showUpdated( $Updated )
  {
    echo "<td style=\"font-size: 8pt; text-align: center;\">" . $Updated . "</td>\n";
  }


//! Display normal table cells
/*! This function outputs a table cell containing the provided
 *  text (only).  it is implemented as a simple function merely
 *  so that style changes can be made easily.
 *
 *  \param $field - Text for the cell
 *  \returns none
 */
function showOthers( $field )
  {
    echo "<td>" . $field . "</td>\n";
  }

//! Display normal table cells - refreshing pages
/*! This function outputs a table cell containing the provided
 *  text.  The cell is displayed in a style used for refreshing
 *  pages.  typically, this is a larger font so that key fields
 *  may be more easily seen in case, for example, the page is
 *  displayed on a wall-mounted screen.
 *
 *  \param $field - Text for the cell
 *  \returns none
 */
function showOthersR( $field )
  {
    echo "<td class=\"tblR\">" . $field . "</td>\n";
  }

?>
