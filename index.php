<?php
/*! \file index.php
 *
 *  \brief SRD menu
 *
 *  This page provides the main menu for the Skywarn Recognition
 *  day log.
 *
 *  \author JJMcD
 *  \date 2013-09-24
 *
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/
include('functions1.inc');
pageHead("Main Menu");

/*! Display a menu entry.
 *  Function simply displays a line of text as a link.  By making
 *  it a function, considerable tweaking can be easily done withoug
 *  a lot of work.
 *
 *  \param $text Text to display
 *  \param $link Address to link to
 *  \returns none
 */
function menuEntry( $text, $link )
{
    echo "        <p><a class=\"menu\" href=\"" . $link . 
         "\">" . $text . "</a></p>\n";
}

/*! Display a menu entry that opens in a new window/tab.
 *  Function simply displays a line of text as a link.  By making
 *  it a function, considerable tweaking can be easily done withoug
 *  a lot of work.  In this variant, the link opens in a new
 *  page or tab (target=).
 *
 *  \param $text Text to display
 *  \param $link Address to link to
 *  \returns none
 */
function menuEntryN( $text, $link )
{
    echo "        <p><a class=\"menu\" href=\"" . $link . 
         "\" target=\"search\">" . $text . "</a>" .
	 "<span style=\"font-size: 10pt\"><br />Opens in new tab/window</span>";
    echo "</p>\n";
}

/*! Display the main menu
 *  Shows a list of the common pages used for logging and
 *  reporting and links to those pages.
 */
{
  echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" " .
       "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
  echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\"" .
       " xml:lang=\"en\">\n";
  echo "  <head>\n";
  echo "    <title>Skywarn Recognition Day Menu</title>\n";
  echo "    <style type=\"text/css\" media=\"all\">@import " .
       "\"default1.css\";</style>\n";
  echo "    <meta http-equiv=\"refresh\" content=\"120\" />\n";
  echo "  </head>\n";
  echo "  <body>\n";
  echo "    <center>\n";
  echo "      <div id=\"MenuArea\">\n";
  echo "        <br />\n";
  echo "        <h2>Skywarn Recognition Day Menu</h2>\n";
  menuEntry("Log contacts",            "selectOp.php" );
  menuEntryN("Search log",             "searchLog.php" );
  menuEntry("Add/Remove Bands",        "addBand.php" );
  menuEntry("Updating Office Grid",    "listOfficesA.php" );
  menuEntry("Updating Office Map",     "makeUpdatingMap.html" );
  menuEntry("View log",                "logView.php" );
  menuentry("Make PDF of log",         "logPDF.php" );

  echo "      </div>\n";
  echo "    </center>\n";
  pageFoot();
}
?>
