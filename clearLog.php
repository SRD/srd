<?php
/*! \file clearLog.php
 *
 *  \brief Delete all log entries
 *
 *  clearLog deleted all entries from `srd_log` and displays
 *  a page indicating this has happened.
 *
 *  \author JJMcD
 *  \date 2013-11-07
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Clear all log entries");

date_default_timezone_set('America/Detroit');

/*! Database handle */
$db=openDatabase();

/*! Delete all log entries SQL query */
$SQL1="DELETE FROM `srd_log`;";
echo "<p class=\"msg\">1: $SQL1 </p>\n";
/*! Result of delete all log entries queries */
$res1=mysql_query($SQL1,$db);
if ( !$res1 )
  echo "<p class=\"msg\">" . mysql_error() . "</p>\n";
else
  echo "<p class=\"msg\"><b>All log entries deleted!</b></p>\n";

 pageFoot();
?>
