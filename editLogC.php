<?php
/*! \file editLogC.php
 *
 *  \brief Edit log entries - update database
 *
 * Accepts form information from editLog.php and updates the
 * database accordingly.
 *
 *  \author JJMcD
 *  \date 2013-10-23
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/


include('functions1.inc');
pageHead("Update Database");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

date_default_timezone_set('America/Detroit');

$logTime = $_GET['logtime']; /*!< Original time of entry */
$opCall = $_GET['opcall']; /*!< Original operator call */
$logband = $_GET['opband']; /*!< Original band */

$statime = $_GET['statime']; /*!< Time from form */
$stacall = strtoupper($_GET['stacall']); /*!< Call from form */
$staloc  = $_GET['staloc']; /*!< Location from form */
$staban  = $_GET['staban']; /*!< Band ID from form */
$stawx   = $_GET['stawx']; /*!< Weather from form */
$stacom  = $_GET['stacom']; /*!< Comments from form */
$staop   = strtoupper($_GET['staop']); /*!< Operator from form */
$staofc  = $_GET['staofc']; /*!< NWS Office ID from form */

/*! Select clause 
   The select clause will be used for the update as well as initially reading
   the record and determining which fields, if any, have been changed */
$selectClause =   "WHERE `srd_time`='" . $logTime . "' AND" .
  "  `srd_band_id` = " . $logband;

/*! SQL query to retrieve original log entry from database */
$SQL1="SELECT * FROM `srd_log` " . $selectClause;
/*! Result from fetching original log entry */
$res1=mysql_query( $SQL1, $db );
/*! Original log entry in database */
$row1=mysql_fetch_row($res1);

/* Typically the user will never see this, but it is useful for deubugging
   by commenting out the header() at the end */
echo "<table>\n";
echo "<tr><td>Log Time: </td><td>" . $row1[0] . "</td><td>" . $statime . "</td>\n";
echo "<tr><td>Call: </td><td>" . $row1[1] .  "</td><td>" . $stacall . "</td>\n";
echo "<tr><td>Location: </td><td>" . $row1[2] .  "</td><td>" . $staloc . "</td>\n";
echo "<tr><td>Band ID: </td><td>" . $row1[3] .  "</td><td>" . $staban . "</td>\n";
echo "<tr><td>Weather: </td><td>" . $row1[4] . "</td><td>" . $stawx .  "</td>\n";
echo "<tr><td>Office: </td><td>" . $row1[5] .  "</td><td>" . $staofc . "</td>\n";
echo "<tr><td>Comments:</td><td> " . $row1[6] .  "</td><td>" . $stacom . "</td>\n";
echo "<tr><td>Operator call:</td><td> " . $row1[7] .  "</td><td>" . $staop . "</td>\n";
echo "<tr><td>Updated: </td><td>" . $row1[8] .  "</td><td>now()</td>\n";
echo "</table>\n";

/* Create the SET clause, including only items changed */

/*! SET clause to update database
 *
 * Since only some fields are likely to have changed, this clause will
 * be constructed piece by piece, depending on which fields the user
 * modified in editLog.php.
 */
$updateClause="SET ";
/*! Remember whether a comma needs to be added */
$comma = 0;
echo "<p>1(" . $updateClause . ")</p>\n";
if ( $row1[0]!=$statime )
  {
    $updateClause = $updateClause . "`srd_time`='" . $statime . "'";
    $comma = 1;
  }
echo "<p>2(" . $updateClause . ")</p>\n";
if ( $row1[1]!=$stacall )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_call`='" . $stacall . "'";
    $comma = 1;
  }
echo "<p>3(" . $updateClause . ")</p>\n";
if ( $row1[2]!=$staloc )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_location`='" . $staloc . "'";
    $comma = 1;
  }
echo "<p>4(" . $updateClause . ")</p>\n";
if ( $row1[3]!=$staban )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_band_id`=" . $staban;
    $comma = 1;
  }
echo "<p>5(" . $updateClause . ")</p>\n";
if ( $row1[4]!=$stawx )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_weather`='" . $stawx . "'";
    $comma = 1;
  }
echo "<p>6(" . $updateClause . ")</p>\n";
if ( $row1[5]!=$staofc )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_nws`=" . $staofc;
    $comma = 1;
  }
echo "<p>7(" . $updateClause . ")</p>\n";
if ( $row1[6]!=$stacom )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_comments`='" . $stacom . "'";
    $comma = 1;
  }
echo "<p>8(" . $updateClause . ")</p>\n";
if ( $row1[7]!=$staop )
  {
    if ( $comma )
      $updateClause = $updateClause . ", ";
    $updateClause = $updateClause . "`srd_opcall`='" . $staop . "'";
    $comma = 1;
  }

/* If no changes, don't touch the database */
if ( $updateClause == "SET " )
  header("Location: doLog.php?call=" . $opCall . "&band=" . $logband ); 

/* User cannot change the updated field, always set to when record updated */
echo "<p>(" . $updateClause . ")</p>\n";
$updateClause = $updateClause . ", `updated`=now() ";
echo "<p>" . $updateClause . "</p>\n";

/* Go execute the update */
/*! SQL query to update the log entry */
$SQL2="UPDATE `srd_log` " . $updateClause . $selectClause . ";";
echo "<p>[" . $SQL2 . "]</p>\n";
/*! Result of updating log entry */
$res2=mysql_query( $SQL2, $db );

/* Return to the logging screen as if nothing happened */
header("Location: doLog.php?call=" . $opCall . "&band=" . $logband ); 

?>
