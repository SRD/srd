<?php
/*! \file editLog.php
 *
 *  \brief Edit log entries
 *
 * Display a form containing the complete record of the requested
 * log entry and allow the user an opportunity to edit it.
 *
 *  \author JJMcD
 *  \date 2013-10-09
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/


include('functions1.inc');
pageHead("Correct Log Entry");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

date_default_timezone_set('America/Detroit');

/*! Time of log entry to change */
$logTime = $_GET['time'];
/*! Call sign of log entry to change */
$opCall = $_GET['op'];
/*! Band of log entry to change */
$logband = $_GET['band'];

/*! Query to get log entry to be changed */
$SQL1="SELECT * FROM `srd_log` " .
  "WHERE " .
  "  `srd_time`='" . $logTime . "' AND" .
//  "  `srd_opcall` = '" . $opCall . "';";
//  "  `srd_opcall` = '" . $opCall . "' AND" .
  "  `srd_band_id` = " . $logband . ";";

/*! Result of query to get log entry */
$res1=mysql_query( $SQL1, $db );
/*! Log entry to change */
$row1=mysql_fetch_row($res1);

/* For for user entry */
echo "<form action=\"editLogC.php\">\n";
echo "<input type=\"hidden\" name=\"opcall\" value=\"" . $opCall . "\">\n"; 
echo "<input type=\"hidden\" name=\"logtime\" value=\"" . $logTime . "\">\n"; 
echo "<input type=\"hidden\" name=\"opband\" value=\"" . $logband . "\">\n"; 
echo "<center>\n";
echo "<table>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Log time</td>\n";
echo "    <td><input type=\"text\" size=\"20\" name=\"statime\" " .
     	  "value=\"" . $row1[0] . "\" /></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Station called</td>\n";
echo "    <td><input type=\"text\" size=\"8\" name=\"stacall\" " .
     	  "value=\"" . $row1[1] . "\" /></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Location</td>\n";
echo "    <td><input type=\"text\" size=\"20\" name=\"staloc\" " .
     	  "value=\"" . $row1[2] . "\" /></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";

/*! Query to get band/mode text from ID */
$SQL3="SELECT band_id, srd_band_a FROM srd_band_a ORDER BY band_id";
/*! Result of get list of bands */
$res3=mysql_query( $SQL3, $db );
echo "    <td class=\"l\">Band</td>\n";
echo "    <td><select name=\"staban\">\n";
/*! Band ID and text */
while ( $row3=mysql_fetch_row($res3) )
{
  if ( $row3[0]==$row1[3] )
      echo "     <option value=\"" . $row3[0] . "\" selected=\"selected\">";
  else
      echo "     <option value=\"" . $row3[0] . "\">";
  echo $row3[1] . "</option>\n";
}
echo "      </select></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Weather</td>\n";
echo "    <td><input type=\"text\" size=\"20\" name=\"stawx\" " .
     	  "value=\"" . $row1[4] . "\" /></td>\n";
//echo "    <td align=\"left\">" . $row1[4] . "</td>\n";
echo "  </tr>\n";
echo "  <tr>\n";

/*! Query to get list of offices */
$SQL4="SELECT srd_office_id,srd_office_code,srd_office_city,srd_office_state" .
 " FROM srd_offices_a " . 
  " ORDER BY srd_office_state, srd_office_city;";
/*! Result of get office list query */
$res4=mysql_query( $SQL4, $db );

echo "    <td class=\"l\">NWS office</td>\n";
echo "    <td><select name=\"staofc\" value=\"" . $row1[5] . "\">\n";
echo "      <option value=\"0\"></option>\n";
/*! Row containing office information */
  while ( $row4=mysql_fetch_row($res4) )
    {
      /*! Text to enter in combo box */
      $entry=$row4[3] . " " . $row4[2] . " " . $row4[1];
      if ( $row4[0]==$row1[5] )
          echo "      <option value=\"" . $row4[0] . 
	       "\" selected=\"selected\" >" . $entry . "</option>\n";
      else
          echo "      <option value=\"" . $row4[0] . "\">" . $entry . 
               "</option>\n";
    }
echo "    </select></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Comments</td>\n";
echo "    <td><input type=\"text\" size=\"20\" name=\"stacom\" " .
     	  "value=\"" . $row1[6] . "\" /></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Operator call</td>\n";
echo "    <td><input type=\"text\" size=\"20\" name=\"staop\" " .
     	  "value=\"" . $row1[7] . "\" /></td>\n";

echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td class=\"l\">Updated</td>\n";
echo "    <td align=\"left\">" . $row1[8] . "</td>\n";
echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td colspan=\"2\"><center><input type=\"submit\" value=\"Update\" /></center></td>\n";
echo "  </tr>\n";
echo "</table>\n";
echo "</center>\n";
echo "</form>\n";
pageFoot();
?>
