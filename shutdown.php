<?php
/*! \file shutdown.php
 *
 *  \brief  Shut down the system
 *
 * @author John J. McDonough, WB8RCR <wb8rcr@arrl.net>
 *
 */

include('functions1.inc');
pageHead("Shut down the SRD server");

echo "<h1 style=\"color: hotpink;\">The system should shut down shortly</h1>\n";

echo "<p class=\"msg1\"><b>&nbsp;&nbsp;Wait for all LED activity to cease before removing power</b></p>\n";
echo "<p class=\"msg\">&nbsp;&nbsp;When ready, only the red LED should be illuminated.<br />\n";
echo "&nbsp;&nbsp;This process may take several minutes.</p>\n";

echo "  </body>\n</html>\n";
file_put_contents("shutdown","shutdown -h now");

?>
