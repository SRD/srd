<?php
/*! \file logView.php
 *
 *  \brief View log
 *
 *  \author JJMcD
 *  \date 2013-09-24
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Skywarn Recognition Day");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

date_default_timezone_set('America/Detroit');

$SQL4="SELECT srd_office_id,srd_office_code,srd_office_city,srd_office_state" .
 " FROM srd_offices_a " . 
  " ORDER BY srd_office_state, srd_office_city;";
$res4=mysql_query( $SQL4, $db );



echo "</div>\n";
$now = time()+5*3600;

?>
<center>

<hr />

<form method="get" name="loglist" action="index.php" >
<table width="95%">
  <tr>
   <th>Time</th>
   <th>Band</th>
   <th>Op</th>
   <th>Call</th>
   <th>Location</th>
   <th>Weather</th>
   <th>Office</th>
   <th>Comments</th>
  <tr>
<?php

$SQL1="SELECT * FROM srd_log " .
  "ORDER BY srd_time " .
  ";";
$res1=mysql_query( $SQL1, $db );
while ( $row1=mysql_fetch_row( $res1 ) )
{
  if ( $row1[5] == 0 )
  {
    $nwsoffice="";
  }
  else
  {
    $SQL3="SELECT srd_office_code FROM srd_offices_a " .
      "WHERE srd_office_id=" . $row1[5];
    $res3=mysql_query( $SQL3, $db );
    $row3 = mysql_fetch_row($res3);
    $nwsoffice = $row3[0];
  }
  $SQL5="SELECT srd_band_a FROM srd_band_a WHERE band_id=" . $row1[3];
  $res5=mysql_query( $SQL5, $db );
  $row5 = mysql_fetch_row($res5);
  $bandtext = $row5[0];
  echo "  <tr>\n";
  echo "    <td>" . substr($row1[0],11,5) . "</td>\n";
  echo "    <td>" . $bandtext . "</td>\n";
  echo "    <td>" . $row1[7] . "</td>\n";
  echo "    <td>" . $row1[1] . "</td>\n";
  echo "    <td>" . $row1[2] . "</td>\n";
  echo "    <td>" . $row1[4] . "</td>\n";
  echo "    <td>" . $nwsoffice . "</td>\n";
  echo "    <td>" . $row1[6] . "</td>\n";
  echo "  <tr>\n";
}
echo "  </table>\n";
//echo "    <p><input type=\"submit\" value=\"Update\" />\n";
echo "      &nbsp;<input type=\"submit\" value=\"Return to Menu\" \n";
echo "      onclick=\"this.form.action='index.php'\"  /></p>\n";
echo "</center>\n";
echo "</form>\n";
pageFoot();
?>

