<?php
/*! \file searchLog.php
 *
 *  \brief Search log entries
 *
 *  \author JJMcD
 *  \date 2013-10-22
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Search Log");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=openDatabase();

date_default_timezone_set('America/Detroit');

/* See if we got here with a call to search */
if (isset($_GET['call']))
  {
    $call = strtoupper($_GET['call']);
  }
else
  {
    $call = 'NOCALL';
  }

/* Form for inputting call to search for */
echo "    </div>\n";
echo "    <p></p>\n";
echo "    <form method=\"get\" name=\"search\" action=\"searchLog.php\" >\n";
echo "      <table>\n";
echo "        <tr>\n";
echo "          <th align=\"right\">Call: &nbsp; </th>\n";
echo "          <td><input type=\"text\" name=\"call\" length=\"8\"/></td>\n";
echo "        </tr>\n        <tr>\n";
echo "          <td colspan=\"2\"><input type=\"submit\" value=\"Submit\" /></td>\n";
echo "        </tr>\n      </table>\n";
echo "    </form>\n";

/* If there is a call to search for, find matching entries */
if ( $call != "NOCALL" )
  {
    echo "    <p></p>\n";
    echo "    <table>\n";
    echo "      <tr>\n";
    echo "        <th>Time</th>\n";
    echo "        <th>Call</th>\n";
    echo "        <th>Location</th>\n";
    echo "        <th>Band</th>\n";
    echo "        <th>Operator</th>\n";
    echo "      </tr>\n";
    $SQL2 = "SELECT A.srd_time,A.srd_opcall,A.srd_band_id," .
            "B.srd_band_a,A.srd_location " .
            "FROM srd_log A, srd_band_a B " .
	    "WHERE (A.srd_band_id=B.band_id) " .
	    "AND (A.srd_call='" . $call . "') " .
	    "ORDER BY srd_time DESC;";
//echo "<p class=\"msg\">(" . $SQL2 . ")</p>\n";
    $res2=mysql_query( $SQL2, $db );
    while ( $row2=mysql_fetch_row($res2) )
      {
        $distime = substr($row2[0],11,5);
	echo "      <tr>\n";
        echo "        <td class=\"l\">" . $distime . "</td>\n";
        echo "        <td class=\"io\">" . $call . "</td>\n";
        echo "        <td class=\"r\">" . $row2[4] . "</td>\n";
        echo "        <td class=\"io\">" . $row2[3] . "</td>\n";
        echo "        <td class=\"io\">" . $row2[1] . "</td>\n";
//        echo "<p>" . $distime . " - " . $call . " - " . $row2[3] .
//             " - " . $row2[1] . "</p>\n";
	echo "      </tr>\n";
      }
    echo "    </table>\n";
  }
pageFoot();
?>