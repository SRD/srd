<?php
/*! \file addBand.php
 *
 *  \brief Add/Remove Active Bands
 *
 *  This page displays two combo boxes.  The first contains the list
 *  of bands that are NOT in use, the second the list of bands that
 *  are in use.  Under each is a Submit button and a return to menu
 *  button.  The Submit button under the first returns the selection
 *  in the 'addband' parameter to addBand1.php.  The second in the
 *  'delband' parameter.
 *
 *  addBand1.php will add or delete the band from the active band
 *  table as appropriate.
 *
 * Pseudocode:
 * \code
 * get all bands from srd_band_all
 * for each band
 *   if NOT in srd_band_a
 *     add to 'add' dropdown
 * get all bands from srd_band_a
 * for each band
 *   add to 'remove' dropdown
 * on submit, addBand1.php
 * \endcode
 *
 *  \author JJMcD
 *  \date 2013-11-06
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Add Active Band");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
//! Database handle
$db=openDatabase();

date_default_timezone_set('America/Detroit');

//! SQL to get list of all bands
$SQL1="SELECT `srd_band_a`,`band_id` FROM `srd_band_all` ORDER BY `band_id`";
//! Result of selecting all bands
$res1=mysql_query( $SQL1, $db );

echo "    </div>\n";
echo "    <center>\n";
echo "    <div>\n";
echo "      <p></p>\n";
echo "      <form method=\"get\" action=\"addBand1.php?add=1\">\n";
echo "        <table width=\"50%\">\n";
echo "          <tr>\n";
echo "            <th align=\"right\" width=\"50%\">Select band to add&nbsp;</th>\n";
echo "            <td class=\"office\"><select name=\"addband\">\n";
echo "                <option value=\"#\">Select Band</option>\n";
//! Each possible band
while ($row1=mysql_fetch_row($res1))
  {
    //! SQL to see if a current band is in the bands in use table
    $SQL2="SELECT `band_id` FROM `srd_band_a` WHERE `band_id` =" .
      $row1[1];
    //! Result of query of in use bands
    $res2=mysql_query( $SQL2,$db );
    //! Not null if this band is in use
    if ( !$row2=mysql_fetch_row($res2) )
      {
	echo "                <option value=\"" . $row1[1] . "\">" . $row1[0] . 
	  "</option>\n";
      }
 }
echo "              </select></td>\n";
echo "          </tr>\n";
echo "      </table>\n";
echo "      <p><input type=\"submit\" value=\"Add Band\" />\n";
echo "        &nbsp;<input type=\"submit\" value=\"Return to Menu\" \n";
echo "        onclick=\"this.form.action='index.php'\"  /></p>\n";
echo "    </form>\n";
echo "  </div>\n";
echo "  </center>\n";

echo "  <div id=\"headarea\">\n";
echo "      <h2>Remove Active Band</h2>\n";
echo "    </div>\n";
echo "    <center>\n";
echo "    <div>\n";
echo "      <p></p>\n";
echo "      <form method=\"get\" action=\"addBand1.php?add=0\">\n";
echo "        <table width=\"50%\">\n";
echo "          <tr>\n";
echo "            <th align=\"right\" width=\"50%\">Select band to remove&nbsp;</th>\n";
echo "            <td class=\"office\"><select name=\"delband\">\n";
echo "                <option value=\"#\">Select Band</option>\n";
//! SQL to select bands in use
$SQL3="SELECT `srd_band_a`,`band_id` FROM `srd_band_a` ORDER BY `band_id`";
//! Result of selecting bands in use
$res3=mysql_query( $SQL3, $db );

//! Specific band in use
while ($row3=mysql_fetch_row($res3))
  {
	echo "                <option value=\"" . $row3[1] . "\">" . $row3[0] . 
	  "</option>\n";
 }
echo "              </select></td>\n";
echo "          </tr>\n";
echo "      </table>\n";
echo "      <p><input type=\"submit\" value=\"Remove Band\" />\n";
echo "        &nbsp;<input type=\"submit\" value=\"Return to Menu\" \n";
echo "        onclick=\"this.form.action='index.php'\"  /></p>\n";
echo "    </form>\n";
echo "  </div>\n";
echo "  </center>\n";

pageFoot();
?>