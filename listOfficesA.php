<?php
/*! \file listOfficesA.php
 *
 *  \brief Display list of NWS offices
 *
 *  Displays a grid of NWS offices grouped by region.  The colors
 *  of the entries depend on whether the office has been contacted.
 *
 *
 * \dot
 * digraph Flow
 * {
 * fontname="Helvetica-bold";
 * label="listOfficesA Flow";
 * node [ style="filled" fillcolor="cornsilk" fontname="Helvetica" ];
 * "for each\nNWS region" [ shape="record" fillcolor="honeydew" ];
 * "for each\nNWS office" [ shape="record" fillcolor="honeydew" ];
 * "next region" [ shape="record" fillcolor="honeydew" ];
 * "next office" [ shape="record" fillcolor="honeydew" ];
 * "Contacted?" [ shape="diamond" fillcolor="beige" ];
 * OpenDatabase->GetRegions;
 * GetRegions->"for each\nNWS region";
 * "for each\nNWS region"->GetOffices;
 * GetOffices->"for each\nNWS office";
 * "for each\nNWS office"->SearchLog;
 * SearchLog->"Contacted?";
 * "Contacted?"->"style=got" [label="yes"];
 * "Contacted?"->"style=no" [label="no"];
 * "style=got"->displayCell;
 * "style=no"->displayCell;
 * displayCell->"next office";
 * "for each\nNWS office"->"next office";
 * "next office"->"next region";
 * "for each\nNWS region"->"next region";
 * }
 * \enddot
 *
 *
 *  \author JJMcD
 *  \date 2013-09-23  
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/
{
    include('functions1.inc');
    pageHeadR("NWS Offices List");
    echo "    </div>\n";

    //-------------------------------------------
    // Open connection to database
    //-------------------------------------------
    /*! Database handle */
    $db=openDatabase();

    /*! Number of columns in grid */
    $columns=6;

    //-------------------------------------------
    // Get list of regions
    //-------------------------------------------

    /*! SQL query to get list of regions */
    $SQL1 = "SELECT srd_region,srd_region_id " . 
      "FROM srd_regions " .
      "ORDER BY srd_region_id";
    /*! Result of get regions query */
    $res1 = mysql_query( $SQL1, $db );

    /*! Region name and ID */
    while ( $row1 = mysql_fetch_row($res1) )
    {
	echo "    <table width=99%>\n";
	echo "      <tr>\n        <th colspan=\"" . 2*$columns . "\">" . 
	  $row1[0] . " Region</th>\n      </tr>\n";
	/*! Count of current column */
    	$colcounter = 0;

        //-------------------------------------------
        // Get list of offices for this region
        //-------------------------------------------

	/*! SQL query to get list of offices in a region */
	$SQL2 = "SELECT srd_office_code, srd_office_city, " .
            "srd_office_state, srd_office_id " .
            " FROM srd_offices_a " .
            " WHERE srd_region=" . $row1[1] . 
	    " ORDER BY srd_office_state, srd_office_city;";
        echo "      <!-- " . $SQL2 . "-->\n";
	/*! Result of get list of offices query */
        $res2 = mysql_query( $SQL2, $db );
	/*! Office code, city, state, ID */
        while ( $row2 = mysql_fetch_row($res2) )
        {
          if ( $colcounter == 0 )
	      echo "      <tr>\n";
	  echo "          <!-- " . $row2[0] . " " . $row2[1] . ", " . $row2[2] .
 	     " -->\n";
          //-------------------------------------------
          // See if any contact in log for this office
          //-------------------------------------------

	  /*! SQL query to count number of contacts with this office */
	  $SQL3 = "SELECT COUNT(*) FROM srd_log WHERE `srd_nws`=" .
	    $row2[3] . ";";
	  /* Result of number of contacts with office query */
    	  $res3 = mysql_query( $SQL3, $db );
	  /*! Number of contacts with current office */
 	  if ( $row3 = mysql_fetch_row($res3) )
	  {
	    if ( $row3[0]>0 )
	      /*! CSS class to use for current cell */
	      $style="got";
	    else
	      $style="no";
	  }
          echo "        <td class=\"" . $style . "\"><b>" . $row2[0] . 
	    "</b></td><td class=\"" . $style . "\">" . 
	    $row2[2] . " " . $row2[1] . "</td>\n";
	  $colcounter = $colcounter + 1;
	  if ( $colcounter == $columns )
          {
	    $colcounter = 0;
	    echo "      </tr>\n";
	  }
        } /* while $row2 */
        if ( $colcounter != 0 )
          echo "      </tr>\n";
        echo "    </table>\n";
    } /* while $row1 */

pageFoot();
}
?>
