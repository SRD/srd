<?php
/*! \file web-root/index.php
 *
 * \brief Redirect page
 *
 * This file resides in the server root and simply directs
 * entries like http://server/ to http://server/srd/index.php
 *
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

header("Location: srd/index.php" );

?>