<?php
/*! \file  logPDF.php
 *
 *  \brief  Generate a pdf of the radio log
 *
 *  This script reads the current log and creates a PDF document
 *  of the log file in chronological order.

\dot
digraph logPDF_flow
{
    fontname="Helvetica-Bold";
    label="logPDF.php Flow";
    node [ style="filled" fillcolor="cornsilk" fontname="Helvetica" ];
    " A " [ shape="circle" style="filled" fillcolor="white" fontname="Helvetica" ];
    "A" [ shape="circle" style="filled" fillcolor="white" fontname="Helvetica" ];
    " B " [ shape="circle" style="filled" fillcolor="white" fontname="Helvetica" ];
    "B" [ shape="circle" style="filled" fillcolor="white" fontname="Helvetica" ];
    "for each entry" [ shape="record" fillcolor="honeydew" ];
    "next entry" [ shape="record" fillcolor="honeydew" ];
    "while weather words left" [ shape="record" fillcolor="honeydew" ];
    "next weather word" [ shape="record" fillcolor="honeydew" ];
    "while comment words left" [ shape="record" fillcolor="honeydew" ];
    "next comment word" [ shape="record" fillcolor="honeydew" ];
    "Near bottom of page?" [ shape="diamond" fillcolor="beige" ];
    "weather>19?" [ shape="diamond" fillcolor="beige" ];
    "comment>19?" [ shape="diamond" fillcolor="beige" ];
    "NWS office?" [ shape="diamond" fillcolor="beige" ];
    "Set up document"->"Fetch Log";
    "Fetch Log"->"for each entry";
    "for each entry"->"Near bottom of page?";
    "A"->"for each entry";
    "Near bottom of page?"->"Do first columns" [label="no"];
    "Near bottom of page?"->"New page" [label="yes"];
    "New page"->"Do first columns";
    "Do first columns"->"Get band text";
    "Get band text"->"Display band text";
    "Display band text"->"Break weather into words";
    "Break weather into words"->"while weather words left";
    "while weather words left"->"weather>19?";
    "weather>19?"->"print weather string" [label="yes"];
    "print weather string"->"add weather word";
    "add weather word"->"next weather word";
    "next weather word"->"while weather words left";
    "weather>19?"->"print remaining weather" [label="no"];
    "next weather word"->"print remaining weather";
    "print remaining weather"->"B";
    "next entry"->" A ";
    " B "->"Break comment into words";
    "Break comment into words"->"while comment words left";
    "while comment words left"->"comment>19?";
    "comment>19?"->"print comment string" [label="yes"];
    "print comment string"->"add comment word";
    "add comment word"->"next comment word";
    "next comment word"->"while comment words left";
    "comment>19?"->"print remaining comment" [label="no"];
    "next comment word"->"print remaining comment";
    "print remaining comment"->"NWS office?";
    "NWS office?"->"Look up office" [label="yes"];
    "NWS office?"->"Display operator" [label="no"];
    "Look up office"->"Display office";
    "Display office"->"Display operator";
    "Display operator"->"next entry";
    "next entry"->"output buffer";
}
\enddot
 */

include ('class.pdf.php');
date_default_timezone_set('America/Detroit');

/*! Create the initial PDF of the document
 *
 * Initialize the page size, PDF information block, etc.  Also
 * sets font to Helvetica.
 *
 * Fixed information added:
 * \li \c Author John J. McDonough
 * \li \c Creator logPDF.php
 * \li \c Keywords Skywarn Recognition Day, NWS, SRD
 * \li \c Title SRD 2013 Log
 * \li \c Subject Radio Log
 *
 * \param $pdf Pointer to the document being created
 * \param $Page_Width Page width in points
 * \param $Page_Height Height of page in points
 * \param $Top_Margin Top of page margin in points
 * \param $Bottom_Margin Page bottom margin in points
 * \param $Left_Margin Left margin in points
 * \param $Right_Margin Right margin in points
 * \returns Pointer to the document
 */
function DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                       $Left_Margin,$Right_Margin)
{
  $PageSize = array(0,0,$Page_Width,$Page_Height);
  $pdf = & new Cpdf($PageSize);

  $PageSize = array(0,0,$Page_Width,$Page_Height);
  $pdf = & new Cpdf($PageSize);

  $pdf->addinfo('Author','John J. McDonough ' . "WB8RCR");
  $pdf->addinfo('Creator','logPDF.php $Revision: 1.1$');
  $pdf->SetKeywords('Skywarn Recognition Day, NWS, SRD');
  $pdf->selectFont('helvetica');

  $pdf->addinfo('Title',_('SRD 2013 Log'));
  $pdf->addinfo('Subject',_('Radio Log') );

  return $pdf;

}

/*! Set up a new page
 *
 * Starts a new page, gives it page number, background, etc.  Places the
 * title at the top of the page
 *
 * \param $pdf Pointer to the document being created
 * \param $PageNumber Page number for the new page
 * \param $Page_Width Page width in points
 * \param $Page_Height Height of page in points
 * \param $Top_Margin Top of page margin in points
 * \param $Bottom_Margin Page bottom margin in points
 * \param $Left_Margin Left margin in points
 * \param $Right_Margin Right margin in points
 * \param $Subtitle Subtitle for the report
 * \returns none
 */
function NextPage($pdf,$PageNumber,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                 $Left_Margin,$Right_Margin,$Subtitle)
{
  $PageNumber++;
  if ($PageNumber>1)
    {
      $pdf->newPage();
    }

  // Pale purple background on printable area
  $pdf->SetFillColor(248,244,255);
  //  $pdf->Rect($Left_Margin,$Top_Margin,$Page_Width-$Left_Margin-$Right_Margin,
  //             $Page_Height-$Top_Margin-$Bottom_Margin,'F');

  // Top line
  $pdf->SetTextColor(0,0,0);
  $pdf->selectFont('helvetica-Bold');
  $FontSize=18;
  $YPos = $Page_Height - $Top_Margin - 19;
  $XPos = $Page_Width/2 - 120;
  $pdf->SetTextColor(0,0,128);
  $pdf->addText($XPos,$YPos,$FontSize,_('Skywarn Recognition Day'));
  $pdf->SetTextColor(0,0,0);
  // Bottom line
  $FontSize=15;
  $YPos = $YPos - 18;
  $XPos = $Page_Width/2 - 80;
  $pdf->addText($XPos,$YPos,$FontSize,$Subtitle);
  
  // Page Number
  $FontSize=8;
  $YPos = $Bottom_Margin + 2;
  if ( $PageNumber & 1 )
    $XPos = $Page_Width - 60;
  else
    $XPos = $Left_Margin + 10;
  $msg = 'Page ' . $PageNumber;
  $pdf->addText($XPos,$YPos,$FontSize,$msg);
  
  $FontSize=10;
  $pdf->selectFont('courier');
}

/*! Place a centered line of text
 *
 * Center a line of text on the document
 *
 * \param $pdf Pointer to the document
 * \param $l Left edge of page
 * \param $r Right edge of page
 * \param $fs Font size
 * \param $text Line of text to display
 */
function centerText($pdf,$l,$r,$y,$fs,$text)
{
  $slen=$pdf->GetStringWidth($text);
  $XPos = round(($l+$r)/2 - $slen/2 );
  //$msg = 'Len ' . $slen . ', y=' . $y . ', text=' . $text;
  //$pdf->addText(200,300,10,$msg);
  //$msg = 'l,r=' . $l . ',' . $r . ', Pos ' . $XPos;
  //$pdf->addText(200,340,10,$msg);
  $pdf->addText($XPos,$y,$fs,$text);
}



  /*! Title for the report */
  $title=_('Skywarn Recognition Day Radio Log');
  
  include('functions1.inc');
  
  /*! Time the page launched */
  $starttime = strftime("%A, %B %d %Y, %H:%M");

  /*! Database handle */
  $db=mysql_connect("localhost","srduser","srdpassword");
  mysql_select_db("SRD",$db);
  if (mysql_errno($db) != 0 )
    {
      prnMsg($ErrorMessage.'<BR>' . mysql_error($db),
	     'error', _('Database Error')); 
    }

  $PaperSize = 'letter'; /*!< Type of paper for the document */
  $Page_Width=612;       /*!< Page width in points */
  $Page_Height=792;      /*!< Height of page in points */
  $Top_Margin=30;        /*!< Top margin in points */
  $Bottom_Margin=57;     /*!< Bottom margin in points */
  $Left_Margin=30;       /*!< Left margin in points */
  $Right_Margin=25;      /*!< Right margin in points */

  /*! Pointer to the document */
  $pdf = DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
		       $Left_Margin,$Right_Margin);
  /*! Page number */
  $PageNumber = 0;
  /*! Subtitle for the document */
  $subtitle = "Radio log for 2013";

  /*! Query to fetch the log */
  $SQL = "SELECT `srd_time`,`srd_call`,`srd_location`,`srd_band_id`," .
    "`srd_weather`,`srd_nws`,`srd_comments`,`srd_opcall` " .
    " FROM `srd_log` " .
    " ORDER BY `srd_time`;";

  /*! Result of the log query */
  $res=mysql_query($SQL, $db);

  /*! Current vertical position on the page */
  $YPos = 0;

  /*! X position for weather column */
  $posWx=220;

  /*! Length to set aside for the band column */
  $lenBand=8;

  /*! Array containing a single log entry */
  while ( $row = mysql_fetch_row($res) )
    {
      if ( $YPos < 100 )
	{
	  NextPage($pdf,$PageNumber,$Page_Width,$Page_Height,
		   $Top_Margin,$Bottom_Margin,
		   $Left_Margin,$Right_Margin,$subtitle);
	  $pdf->addJpegFromFile('/var/www/html/srd/NWS01c.jpg',$Left_Margin,
				$Page_Height-$Top_Margin-50,
				70,70);
	  $pdf->SelectFont("helvetica-Bold");

	  /*! Font size of log entries */
	  $FontSize = 12;

	  //	centerText($pdf,$Left_Margin,$Page_Width-$Right_Margin,
	  //		   $Page_Height-$Top_Margin-40,$FontSize,
	  //		   $subtitle);
	  $YPos = $Page_Height-$Top_Margin-70;
	  $pdf->addText($Left_Margin+5,$YPos,$FontSize,"Time");
	  $pdf->addText($Left_Margin+40,$YPos,$FontSize,"Call");
	  $pdf->addText($Left_Margin+90,$YPos,$FontSize,"Location");
	  $pdf->addText($Left_Margin+170,$YPos,$FontSize,"Band");
	  $pdf->addText($Left_Margin+$posWx,$YPos,$FontSize,"Weather");
	  $pdf->addText($Left_Margin+340,$YPos,$FontSize,"Notes");
	  $pdf->addText($Left_Margin+460,$YPos,$FontSize,"Office");
	  $pdf->addText($Left_Margin+500,$YPos,$FontSize,"Op");
	  $pdf->SetDrawColor(96,112,128);
	  $pdf->Line( $Left_Margin, $YPos-3, 
		      $Page_Width-$Left_Margin-$Right_Margin,$YPos-3);
	  $pdf->SetDrawColor(0,0,0);
	  $YPos -= 14;
	  $pdf->SelectFont("helvetica");
	  $FontSize = 10;

	  /*! Font size for operator call column */
	  $NCSfont = 8;

	  $PageNumber++;
	}
      //    $pdf->Line( $Left_Margin, $YPos, 
      //		$Page_Width-$Left_Margin-$Right_Margin,
      //		$YPos);
      $pdf->addText($Left_Margin+5,$YPos,$FontSize,substr($row[0],11,5));
      $pdf->addText($Left_Margin+40,$YPos,$FontSize,$row[1]);
      $pdf->addText($Left_Margin+90,$YPos,$FontSize,$row[2]);

      /*! Query to get band text from band ID */
      $SQL5="SELECT srd_band_a FROM srd_band_a WHERE band_id=" . $row[3];

      /*! Result of band text query */
      $res5=mysql_query( $SQL5, $db );

      /*! Text description of band */
      $row5 = mysql_fetch_row($res5);

      /*! Displayed version of band text */
      $bandtext = substr($row5[0],0,$lenBand);

      $pdf->addText($Left_Margin+170,$YPos,$FontSize,$bandtext);
      //$pdf->addText($Left_Margin+210,$YPos,$FontSize,$row[4]);

      /*! Array of words in a long field */
      $words = explode(" ",$row[4],200);

      /*! Count of words in a longer field */
      $i = 0;

      /*! Current line of a word-wrapped field */
      $string = "";

      /*! Temporary vertical position during word wrapping */
      $temp1YPos = $YPos; 

      while ( $words[$i] )
	{
	  if ( strlen($string) > 19 )
	    {
	      $pdf->addText($Left_Margin+$posWx,$temp1YPos,$FontSize,$string);
	      $string = "";
	      $temp1YPos -= 14;
	    }
	  $string = $string . $words[$i] . " ";
	  $i++;
	}
      $pdf->addText($Left_Margin+$posWx,$temp1YPos,$FontSize,$string);

      $words = explode(" ",$row[6],200);
      $i = 0;
      $string = "";
      while ( $words[$i] )
	{
	  if ( strlen($string) > 19 )
	    {
	      $pdf->addText($Left_Margin+340,$YPos,$FontSize,$string);
	      $string = "";
	      $YPos -= 14;
	    }
	  $string = $string . $words[$i] . " ";
	  $i++;
	}
      $pdf->addText($Left_Margin+340,$YPos,$FontSize,$string);
      if ( $temp1YPos < $YPos )
	$YPos = $temp1YPos;
      /* If the contact was with an NWS office */
      if ( $row[5]>0 )
	{

	  /*! Query to get NWS office code by ID */
	  $SQL3="SELECT srd_office_code FROM srd_offices_a " .
	    "WHERE srd_office_id=" . $row[5];

	  /*! Result of NWS office query */
	  $res3=mysql_query( $SQL3, $db );

	  /*! Row containing NWS office code */
	  $row3 = mysql_fetch_row($res3);

	  /*! NWS office 3 letter code */
	  $nwsoffice = $row3[0];

	  $pdf->addText($Left_Margin+465,$YPos,$FontSize,$nwsoffice);
	}
      $pdf->addText($Left_Margin+492,$YPos,$NCSfont,$row[7]);
      $pdf->SetDrawColor(192,224,255);
      $pdf->Line( $Left_Margin, $YPos-3, 
		  $Page_Width-$Left_Margin-$Right_Margin,$YPos-3);
      $pdf->SetDrawColor(0,0,0);
      $YPos -= 14;
    }

  /*! Output buffer */
  $buf = $pdf->output();

  /*! Length of output buffer */
  $buflen += strlen($buf);

  header('Content-type: application/pdf');
  header('Content-Length: ' . $buflen);
  header('Content-Disposition: inline; filename=' . 'SRDlog.pdf');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');

  $pdf->stream()

?>