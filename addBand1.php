<?php
/*! \file addBand1.php
 *
 *  \brief Add or remove a band from the active band table
 *
 *  addBand1.php checks to see if the 'addband' parameter is present.
 *  If it is, it will get the band text for that ID from srd_band_all
 *  and insert the record into srd_band_a.
 *
 *  It then checks for the 'delband' parameter.  (Only one of the two
 *  parameters will ever be present, although addBand1.php can deal
 *  with both.)  addBand1 will then delete the record with the
 *  provided ID from the srd_band_a table.
 *
 *  When complete, control is returned to index.php
 *
 * Pseudocode:
 * \code
 * if addband
 *   get band description from srd_band_all
 *   insert into srd_band_a
 * if delband
 *   delete from srd_band_a
 * return to index.php
 * \endcode
 *
 *  \param addband - ID of a band to add to the active band table
 *  \param delband - ID of a band to remove from the active band table
 *
 *  \author JJMcD
 *  \date 2013-11-06
 *
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Skywarn Recognition Day");

date_default_timezone_set('America/Detroit');

//! Database handle
$db=openDatabase();

//! ID of the band to add to the active band table
if ( $bandid = $_GET['addband'] )
  {
    //! SQL to get band description from the all bands table
    $SQL1="SELECT `srd_band_a` FROM `srd_band_all` WHERE `band_id`=" .
      $bandid;
    echo "<p class=\"msg\">1: $SQL1 </p>\n";
    //! Result of band text query
    $res1=mysql_query($SQL1,$db);
    if ( !$res1 )
      echo "<p class=\"msg\">" . mysql_error() . "</p>\n";
    //! Row containing band text
    if ( $row1=mysql_fetch_row($res1) )
      {
	//! SQL to insert new band into active band table
	$SQL2="INSERT INTO `srd_band_a` VALUES(" . $bandid . ",'" .
	  $row1[0] . "',now());";
	echo "<p class=\"msg\">2: $SQL2 </p>\n";
	//! Result of inserting new band
	$res2=mysql_query($SQL2,$db);
	if ( !$res2 )
	  echo "<p class=\"msg\">" . mysql_error() . "</p>\n";
      }
  }

//! ID of the band to delete from the active band table
if ( $bandid = $_GET['delband'] )
  {
    //! SQL to delete a band from the active band table
    $SQL3="DELETE FROM `srd_band_a` WHERE `band_id`=" . $bandid;
    echo "<p class=\"msg\">3: $SQL3 </p>\n";
    //! Result of delete query
    $res3=mysql_query($SQL3,$db);
    if ( !$res3 )
      echo "<p class=\"msg\">" . mysql_error() . "</p>\n";
  }
header("Location: index.php");
?>