<?php
/*! \file makeMap.php
 *
 *  \brief Create map of NWS offices worked worked
 *
 *  This script serves a png image showing a map of the
 *  United States with a dot for each NWS office.  Offices
 *  outside the lower 48 are shown in a table in the lower
 *  left corner.  If a station has been worked the dot is
 *  a light red; if not a dark green.
 *
 *  \author John J. McDonough, WB8RCR
 *  \date 2013-10-07
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

//include('includes/session.inc');
include('functions1.inc');

/*! Draw a filled ellipse on the image
 *
 * Draw a circle (10x10 filled ellipse) at the coordinates
 * of the image that correspond to the specifice latitude
 * and longitude.
 *
 * \param $image Pointer to image to draw on
 * \param $lat Latitude for the point
 * \param $lon Longitude for the point
 * \param $color Color to draw the point
 */
function makeDot( $image, $lat, $lon, $color )
{
  $x = (126.+$lon) * 14.95;
  $y = (50.5-$lat) * 19.0;
  imagefilledellipse( $image, $x, $y, 10, 10, $color );
}

// Open the database
/*! Database handle */
$db = openDatabase();
mysql_select_db( 'SRD', $db );

// Create the background wallpaper
/*! Pointer to the image being created */
$image = imagecreatefrompng("us_mercator_h600d1.png");
/*! Color for unworked offices */
$unworked = imagecolorallocate( $image, 20,60,20 );
/*! Color for text names of non-continental offices */
$textcolor = imagecolorallocate( $image, 192,192,255 );
/*! Color for worked utilities */
$worked= imagecolorallocate( $image, 255,50,50 );

/*! SQL Query to get NWS office locations */
$SQL1="SELECT srd_office_x,srd_office_y FROM srd_offices_a;";
/*! Result of office locations query */
$res1 = mysql_query($SQL1,$db);
/*! NWS office latitude and longitde */
while($row1=mysql_fetch_row($res1))
{
  /* Note that non-continental offices will be off the map */
  makeDot( $image, $row1[1], $row1[0], $unworked );
}
/* Now draw the dots for those offices that aren't in the lower 48 */
imagefilledellipse( $image, 30, 480, 10, 10, $unworked );
ImageString( $image, 5, 40, 473, "Pago Pago", $textcolor );
imagefilledellipse( $image, 30, 455, 10, 10, $unworked );
ImageString( $image, 5, 40, 448, "Barrigada", $textcolor );
imagefilledellipse( $image, 30, 430, 10, 10, $unworked );
ImageString( $image, 5, 40, 423, "Carolina", $textcolor );
imagefilledellipse( $image, 30, 405, 10, 10, $unworked );
ImageString( $image, 5, 40, 398, "Honolulu", $textcolor );
imagefilledellipse( $image, 150, 480, 10, 10, $unworked );
ImageString( $image, 5, 160, 473, "Fairbanks", $textcolor );
imagefilledellipse( $image, 150, 455, 10, 10, $unworked );
ImageString( $image, 5, 160, 448, "Anchorage", $textcolor );
imagefilledellipse( $image, 150, 430, 10, 10, $unworked );
ImageString( $image, 5, 160, 423, "Juneau", $textcolor );

/*! SQL Query to get log entries */
$SQL2="SELECT srd_nws FROM srd_log WHERE srd_nws IS NOT NULL;";
/*! Result of log query */
$res2 = mysql_query($SQL2,$db);
/*! NWS office ID field from log */
while($row2=mysql_fetch_row($res2))
{
  $x = $y = 0;
  /* Specify locations for offices outside the lower 48 */
  switch ( $row2[0] )
  {
    case 80:
      $x = 30;
      $y = 430;
      break;
    case 118:
      $x = 150;
      $y = 455;
      break;
    case 119:
      $x = 150;
      $y = 480;
      break;
    case 120:
      $x = 150;
      $y = 430;
      break;
    case 121:
      $x = 30;
      $y = 405;
      break;
    case 122:
      $x = 30;
      $y = 455;
      break;
    case 123:
      $x = 30;
      $y = 480;
      break;
    default:
      /* For lower 48 offices, get location from the database */
      /*! SQL query to get office location by ID */
      $SQL3="SELECT srd_office_x,srd_office_y FROM srd_offices_a " . 
        "WHERE srd_office_id=" . $row2[0] . ";";
      /*! Result of get office location query */
      $res3 = mysql_query($SQL3,$db);
      /*! Office latitude and longitude */
      $row3 = mysql_fetch_row($res3);
      makeDot( $image, $row3[1], $row3[0], $worked );
  }
  if ( ($x!=0) && ($y!=0) )
    imagefilledellipse( $image, $x, $y, 10, 10, $worked );
}

// Finally, actually expose the image
header('Content-type: image/png');
ImagePNG($image);

?>
