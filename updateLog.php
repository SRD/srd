<?php
/*! \file updateLog.php
 *
 *  \brief Insert an entry into the log database
 *
 *  Accepts a group of fields:
 *  \li \c operator \c call
 *  \li \c band/mode \c ID
 *  \li \c call
 *  \li \c location
 *  \li \c weather
 *  \li \c NWS \c Office
 *  \li \c comments
 *
 *  and updates the log record appropriately.
 *
 *  \c opcall and \c bandid are required to get back to
 *  \c doLog.php still set up for operator.
 *
 *  \author John J. McDonough, WB8RCR
 *  \date 2013-10-13
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Skywarn Recognition Day");

/*! Operator call */
$opcall = strtoupper($_GET['opcall']);
/*! Band/mode ID */
$bandid = $_GET['bandid'];
/*! Call sign of the stations called */
$call = strtoupper($_POST['call']);
/*! Location of the station called */
$loc = $_POST['location'];
/*! Weather report from the called station */
$wx = $_POST['weather'];
/*! NWS office of station called */
$nws = $_POST['office'];
/*! Comments on QSO */
$comments = $_POST['comments'];

date_default_timezone_set('America/Detroit');

/*! Database handle */
$db=openDatabase();

echo "<p>Operator:" . $opcall . " Call:" . $call . " Loc:" . $loc . " Band:" . $bandid . "</p>\n";
echo "<p>Wx:" . $wx . " NWS:" . $nws . " Cmts:" . $comments . "</p>\n";

/* Go through each field and if absent, use "NULL" */
if ( $nws==0 )
  /*! Text for NWS field */
  $nwsText = "NULL";
else
  $nwsText = $nws;
echo "<p>nwsText = " . $nwsText . "</p>\n";

/* Weather and comment fields must be purged of apostrophes */
if ( $wx != NULL )
  /*! Text for weather field */
  $wxText = "'" . strtr($wx,array("'"=>"\'",'_'=>'\_','%'=>'\%')) . "'";
else
  $wxText = "NULL";
echo "<p>wxText = " . $wxText . "</p>\n";

if ( $comments )
  /*! Text for comment field */
  $comText = "'" . strtr($comments,array("'"=>"\'",'_'=>'\_','%'=>'\%')) . "'";
else
  $comText = "NULL";
echo "<p>ComText = " . $comText . "</p>\n";

/*! Time to use for QSO time */
$logtime =  strftime("%Y-%m-%d %H:%M:%S",time()+5*3600);
echo "<p>Time='" . $logtime . "'</p>\n";
/*! SQL query to insert record into log */
$SQL="INSERT INTO srd_log VALUES('" . $logtime . "','" . $call . "','" . $loc . "'," . $bandid . "," . $wxText . "," . $nwsText . "," . $comText . ",'" . $opcall . "',now());";
echo "<p>Query='" . $SQL . "'</p>\n";
/*! Result of INSERT statement */
$res=mysql_query($SQL,$db);

echo "<p><b>" . $SQL . "</b></p>\n";

$locstr="Location: doLog.php?call=" . $opcall . "&band=" . $bandid;
header($locstr);
